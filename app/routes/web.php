<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->group(['prefix' => 'api/v1/'], function($router){
    $router->group(['prefix' => 'account/'], function($router){
        $router->group(['middleware' => 'auth:api'], function($router) {
            $router->get('', ['as' => 'account', 'uses' => 'AccountController@account']);
        });
        $router->post('login', ['as' => 'account.login', 'uses' => 'AccountController@login']);
    });

    $router->group(['prefix' => 'users/'], function($router){
        $router->post('create', ['as' => 'users.create', 'uses' => 'UsersController@storeUser']);

        $router->group(['middleware' => 'auth:api'], function($router){
            $router->get('', ['as' => 'users.all', 'uses' => 'UsersController@allUsers']);
            $router->get('{id}', ['as' => 'users.get', 'uses' => 'UsersController@getUser']);
        });
    });
});
