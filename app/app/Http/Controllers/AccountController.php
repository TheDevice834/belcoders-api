<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AccountController extends Controller
{
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $user = User::where('email', $request->input('email'))->first();

        if($user !== null && Hash::check($request->input('password'), $user->password)){
            $api_token = Str::random(60);
            $user->update([
                'api_token' => $api_token
            ]);

            return response()->json([
                'status' => 'OK',
                'api_token' => $api_token
            ], 200);
        } else {
            return response()->json([
                'status' => 'FAIL',
                'message' => 'Invalid login or password.'
            ], 401);
        }
    }

    public function account(Request $request)
    {
        return response()->json($request->user());
    }
}
