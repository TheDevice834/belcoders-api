<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    private $json_except;

    public function __construct()
    {
        $this->json_except = [
            'api_token', 'updated_at', 'email'
        ];
    }

    public function allUsers(Request $request)
    {
        $users = User::all();

        foreach($users as $key => $user){
            foreach($this->json_except as $except){
                unset($users[$key][$except]);
            }
        }

        return response()->json([
            'status' => 'OK',
            'response' => $users
        ], 200);
    }

    public function storeUser(Request $request)
    {
        $messages = [
            'required' => 'Поле :attribute не заполнено.',
            'unique' => ':attribute :input уже занят.',
            'between' => 'Длина поля :attribute должна быть не меньше :min и не больше :max символов.',
            'max' => 'Длина поля :attribute должна быть не больше :max символов.',
            'email' => 'Введите корректный :attribute.',
        ];

        $attributes = [
            'username' => 'Логин',
            'email' => 'E-Mail',
            'password' => 'Пароль',
        ];

        $this->validate($request, [
            'username' => 'required|unique:users|between:4,24',
            'email' => 'required|email|unique:users|max:120',
            'password' => 'required|between:6,64'
        ], $messages, $attributes);

        $user = new User;
        $user->fill([
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password'))
        ])->save();

        return response()->json([
            'status' => 'OK',
            'user' => [
                'id' => $user->id,
                'username' => $user->username
            ]
        ], 200);
    }

    public function getUser(Request $request, $id)
    {
        $user = User::find($id);

        if($user === null) return response()->json([
            'status' => 'error',
            'message' => 'user not found'
        ], 404);

        foreach($this->json_except as $except){
            unset($user->$except);
        }

        return response()->json([
            'status' => 'OK',
            'response' => $user
        ], 200);
    }
}
