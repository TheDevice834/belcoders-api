<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UsersTest extends TestCase
{
    use DatabaseTransactions;

    public function testAllUsers()
    {
        $route = route('users.all');

        $response = $this->call('GET', $route);
        $this->assertEquals(401, $response->status());

        $user = factory('App\User')->create();

        $response = $this->call('GET', $route.'?api_token='.$user->api_token);
        $this->assertEquals(200, $response->status());
    }

    public function testStoreUser()
    {
        $route = route('users.create');
        $response = $this->call('POST', $route, [
            'username' => 'user'
        ]);
        $this->assertEquals(422, $response->status());

        $response = $this->call('POST', $route, [
            'email' => 'invalid',
            'password' => 'pass',
            'username' => 'usr'
        ]);
        $this->assertEquals(422, $response->status());

        $response = $this->call('POST', $route, [
            'email' => 'user@belcoders.ru',
            'password' => 'validpass',
            'username' => 'user'
        ]);
        $this->assertEquals(200, $response->status());

        $response = $this->call('POST', $route, [
            'email' => 'user@belcoders.ru',
            'password' => 'validpass',
            'username' => 'user'
        ]);
        $this->assertEquals(422, $response->status());
    }

    public function testGetUser(){
        $user = factory('App\User')->create();
        $route = route('users.get', ['id' => $user->id]);
        $nonexistent_route = route('users.get', ['id' => 0]);

        $response = $this->call('GET', $route);
        $this->assertEquals(401, $response->status());

        $response = $this->call('GET', $nonexistent_route.'?api_token='.$user->api_token);
        $this->assertEquals(404, $response->status());

        $response = $this->call('GET', $route.'?api_token='.$user->api_token);
        $this->assertEquals(200, $response->status());
    }
}
