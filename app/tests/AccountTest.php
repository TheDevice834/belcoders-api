<?php

use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class AccountTest extends TestCase
{
    use DatabaseTransactions;

    public function testLogin(){
        $route = route('account.login');

        $response = $this->call('POST', $route, [
            'username' => 'user'
        ]);
        $this->assertEquals(422, $response->status());

        $password = '123456';
        $user = factory('App\User')->create([
            'password' => Hash::make($password)
        ]);

        $response = $this->call('POST', $route, [
            'email' => $user->email,
            'password' => $password
        ]);
        $this->assertEquals(200, $response->status());
    }

    public function testAccount(){
        $route = route('account');

        $response = $this->call('GET', $route);
        $this->assertEquals(401, $response->status());

        $user = factory('App\User')->create();
        $response = $this->call('GET', $route.'?api_token='.$user->api_token);
        $this->assertEquals(200, $response->status());
    }
}
